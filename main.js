var parsedJson = "";
var xhttp = new XMLHttpRequest();

xhttp.onreadystatechange = function(){
  if(this.readyState == 4 && this.status) {
    parsedJson = JSON.parse(this.responseText)['watches'];
    console.log(parsedJson);
  }
}

xhttp.open("GET","https://raw.githubusercontent.com/Mewtwo2/watch-data/master/watch.json",true);
xhttp.send();

var casioDetails = document.getElementById('casio-button');
var casioList = document.getElementById('casio-ul');

var timexDetails = document.getElementById('timex-button');
var timexList = document.getElementById('timex-ul');

var rolexDetails = document.getElementById('rolex-button');
var rolexList = document.getElementById('rolex-ul');

casioDetails.addEventListener("click",function(){
  console.log(parsedJson['casio']);

  let brandItem = document.createElement('li');
  brandItem.innerHTML = parsedJson['casio']['make'];
  casioList.appendChild(brandItem);

  let modelListItem = document.createElement('li');
  modelListItem.innerHTML = parsedJson['casio']['model'];
  casioList.appendChild(modelListItem);

  let typeListItem = document.createElement('li');
  typeListItem.innerHTML = parsedJson['casio']['type'];
  casioList.appendChild(typeListItem);

});

timexDetails.addEventListener("click",function(){
  console.log(parsedJson['timex']);

  let brandItem = document.createElement('li');
  brandItem.innerHTML = parsedJson['timex']['make'];
  timexList.appendChild(brandItem);

  let modelListItem = document.createElement('li');
  modelListItem.innerHTML = parsedJson['timex']['model'];
  timexList.appendChild(modelListItem);

  let typeListItem = document.createElement('li');
  typeListItem.innerHTML = parsedJson['timex']['type'];
  timexList.appendChild(typeListItem);

});

rolexDetails.addEventListener("click",function(){
  console.log(parsedJson['rolex']);

  let brandItem = document.createElement('li');
  brandItem.innerHTML = parsedJson['rolex']['make'];
  rolexList.appendChild(brandItem);

  let modelListItem = document.createElement('li');
  modelListItem.innerHTML = parsedJson['rolex']['model'];
  rolexList.appendChild(modelListItem);

  let typeListItem = document.createElement('li');
  typeListItem.innerHTML = parsedJson['rolex']['type'];
  rolexList.appendChild(typeListItem);

});
